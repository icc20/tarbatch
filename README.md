# tarbatch

A script that indexes a tree to allow the creation of a set of independent tar files

This script attempts to find all files (not directories) under the input folder
and group them into batches that fit under the given maximum size, without
splitting any of the files. If a file exceeds the threshold the script will
issue a warning, but continue.

This means that tar archives made from this information can be extracted
separately without having to read many terabytes of data in order to access a
few small files.

As the archival process is computationally intensive (especially with
compression) the script also creates a GNU make Makefile, to aid in
parallelisation. To parallelise across hosts with a shared filesystem batch or
queue managers can be used. See para-make for an example.
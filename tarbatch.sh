#!/bin/bash

# Concept cribbed from 
#  https://superuser.com/questions/189691/how-to-split-a-tar-file-into-smaller-parts-at-file-boundaries

function usage {
  echo -e "$0 -i <in> -o <out> -s <max> [ -m <numjobs> ] [-J | -j | -z | -Z ] [ -h ]\n"
  echo -e "\tin: Source directory"
  echo -e "\tout: Directory to put tar files (empty please!)"
  echo -e "\tmax: Split threshold (IEC format)"
  echo -e "\t-m: Run GNU make with <numjobs> jobs"
  echo -e "\t-J: xz\t-j: bzip2\t-z: gzip\t-Z: zip (otherwise uncompressed)"
  echo -e "\t-q: quiet, warnings only"
}

function docs {
  usage
  cat <<_DOCS_

This script attempts to find all files (not directories) under the input folder
and group them into batches that fit under the given maximum size, without
splitting any of the files. If a file exceeds the threshold the script will
issue a warning, but continue.

This means that tar archives made from this information can be extracted
separately without having to read many terabytes of data in order to access a
few small files.

As the archival process is computationally intensive (especially with
compression) the script also creates a GNU make Makefile, to aid in
parallelisation. To parallelise across hosts with a shared filesystem batch or
queue managers can be used. See para-make for an example.

20190719 icc20@gen.cam.ac.uk 
_DOCS_
}

if [ "$1" == "--help" ]; then
  docs
  exit 0
fi

ARGS=$(getopt i:o:s:m:JjzZqh $*)
if [ $? -ne 0 ]; then
  usage
  exit 1
fi

# Set defaults
ARCSUFFIX="tar"
TARFLAGS="cvf"
JOBS=0
unset Q IN OUT MAX

if echo $OSTYPE | grep -q "freebsd" ;then
  NUMFMT=gnumfmt
  DUINK="du -sAk"
  MAKE=gmake
  REALPATH=grealpath
else
  NUMFMT=numfmt
  DUINK="du -sk --apparent-size"
  MAKE=make
  REALPATH=realpath
fi

# Parse args
set -- $ARGS
while :; do
  case "$1" in
    -i)
      IN=$2
      shift
      shift
      ;;
    -o)
      OUT=$2
      shift
      shift
      ;;
    -s)
      MAX=$(($($NUMFMT --from=iec $2)/1024))
      if [ $? -ne 0 ]; then
	usage
	exit 1
      fi
      shift
      shift
      ;;
    -m)
      JOBS=$2
      shift
      shift
      ;;
    -J)
      ARCSUFFIX="tar.xz"
      TARFLAGS="cvJf"
      shift
      ;;
    -j)
      ARCSUFFIX="tar.bz2"
      TARFLAGS="cvjf"
      shift
      ;;
    -z)
      ARCSUFFIX="tar.gz"
      TARFLAGS="cvzf"
      shift
      ;;
    -Z)
      ARCSUFFIX="zip"
      TARFLAGS="cvf"
      shift
      ;;
    -q)
      Q=1
      shift
      ;;
    -h)
      docs
      exit 0
      ;;
    --)
      shift; break
  esac
done

# Check args
if [ -z "$IN" -o -z "$OUT" -o -z "$MAX" ]; then
  usage
  exit 1
fi

if [ ! -e "$OUT" ] ; then
  echo "Warning: Making output dir"
  mkdir -p "$OUT"
fi
if [ ! -d "$OUT" ] ; then
  echo "Error: $OUT is not a directory"
  exit 2
fi
if [ -e "$OUT/index.tsv" ] ; then
  echo "Error: Index from previous run detected. Clean or use a different output directory"
  exit 2
fi

function make_listfiles () {
  [ ! $Q ] && echo "Sequence  Total  Size/Tgt   Files" > /dev/stderr
  # Prep loop variables
  unset SIZE NAME
  SEQ=0 TOT=0 FILES=0 GRANDTOT=0
  SEQFILE=$(printf '%08d' $SEQ)
  #echo -n "tars: " >> "$OUT/Makefile"
  while read SIZE NAME ; do
    if [ $TOT != 0 ] && [ $((TOT+SIZE)) -gt $MAX ] ; then
      [ ! $Q ] && printf '\r%08d: %5s %5s/%-5s %d\n' $SEQ $($NUMFMT --from=iec --to=iec ${GRANDTOT}K) $($NUMFMT --from=iec --to=iec ${TOT}K) $($NUMFMT --from=iec --to=iec ${MAX}K) $FILES > /dev/stderr
      # Append to the Makefile
#      echo -n "$SEQFILE.$ARCSUFFIX " >> "$OUT/Makefile"
      SEQ=$((SEQ+1)) TOT=0 FILES=0
      SEQFILE=$(printf '%08d' $SEQ)
    fi
    TOT=$((TOT+SIZE))
    GRANDTOT=$((GRANDTOT+SIZE))
    FILES=$((FILES+1))
    if [ $(($FILES % 1000)) == 0 ] ; then
      [ ! $Q ] && printf '\r%08d: %5s %5s/%-5s %d' $SEQ $($NUMFMT --from=iec --to=iec ${GRANDTOT}K) $($NUMFMT --from=iec --to=iec ${TOT}K) $($NUMFMT --from=iec --to=iec ${MAX}K) $FILES > /dev/stderr
    fi
    if [ $SIZE -gt $MAX ] ; then
      echo "Warning: File $NAME ($($NUMFMT --from=iec --to=iec ${SIZE}K)) is bigger than maximum chunk size" | tee -a $OUT/errors.log > /dev/stderr
    elif [ $SIZE -gt $(( $MAX/10 )) ] && [[ $NAME == *.zip || $NAME == *.gz || $NAME == *.bz2 || $NAME == *.rar || $NAME == *.xz ]]; then
      echo "Warning: Existing large compressed file $NAME ($($NUMFMT --from=iec --to=iec ${SIZE}K))" | tee -a $OUT/errors.log > /dev/stderr
    fi
    if [ ! -r "$NAME" ] ; then
      echo "Warning: Cannot read $NAME, fix this before proceeding" | tee -a $OUT/errors.log > /dev/stderr
    fi
    # TODO some filenames are bad and need to be escaped. This is a pain. Perhaps in outoing pipe?
    # Put the file into the index
    echo -e "$SEQFILE.$ARCSUFFIX\t${NAME}" #>> "$OUT/index.tsv"
    #printf "%s.%s\t%s\n" $SEQFILE $ARCSUFFIX "${NAME@Q}" >> "$OUT/index.tsv"
  done
  #echo "$SEQFILE.$ARCSUFFIX " >> "$OUT/Makefile"
  [ ! $Q ] && printf '\r%08d: %5s %5s/%-5s %d\n' $SEQ $($NUMFMT --from=iec --to=iec ${GRANDTOT}K) $($NUMFMT --from=iec --to=iec ${TOT}K) $($NUMFMT --from=iec --to=iec ${MAX}K) $FILES > /dev/stderr
}

# could be running make on another machine with different file system heirarchy
RELIN=$($REALPATH --relative-to=. $IN)
ln -s $($REALPATH --relative-to=$OUT $IN) $OUT/$RELIN

# make the makefile
cat > "$OUT/Makefile"  <<"_MAKEFILE_"
SHELL=/bin/bash
TARFILES ?= $(shell cut -f 1 index.tsv | uniq )
SED ?= $(shell type gsed > /dev/null && echo gsed || echo sed)
TAR ?= $(shell type gtar > /dev/null && echo gtar || echo tar)
AWK ?= $(shell type gawk > /dev/null && echo gawk || echo awk)
dst ?= /dev/null

PARAHOSTS=..
PARAOPTS+="-M --sshdelay 0.2 --wd . --nice 10 --progress --halt=soon,fail=1"

.DELETE_ON_ERROR:
.PHONY: search test extract help paratars paraupload contenttest paracontenttest parextract

help:
	@echo "Usage "
	@echo " make search pattern=?           -- search the index"
	@echo " make extract pattern=? dst=?    -- extract files one by one"
	@echo " ---"
	@echo " make tars                       -- make the tar files using make"
	@echo " make test                       -- check tar log against index.tsv"
	@echo " make contenttest                -- check tar contents against index.tsv"
	@echo " make upload dst=?               -- rsync all tars etc"
	@echo " ---"
	@echo " make paratars                   -- make the tar files using parallel"
	@echo "                                    across multiple hosts"
	@echo " make parupload dst=?            -- rsync upload"
	@echo " make parextract pattern=? dst=? -- extract with parallel (one host)"
	@echo " make paracontenttest            -- test the tar files using parallel"
	@echo "                                    across multiple hosts"

%.tar.xz:
	$(TAR) -cvJf $@ --numeric-owner -T <( $(AWK) -F '\t' '/^$@/ { print $$2 }' < index.tsv ) | $(AWK) -v tar=$@ 'BEGIN { OFS="\t" } { print tar,$$0 }' > $@.contents
%.tar.bz2:
	$(TAR) -cvjf $@ --numeric-owner -T <( $(AWK) -F '\t' '/^$@/ { print $$2 }' < index.tsv ) | $(AWK) -v tar=$@ 'BEGIN { OFS="\t" } { print tar,$$0 }' > $@.contents
%.tar.gz:
	$(TAR) -cvzf $@ --numeric-owner -T <( $(AWK) -F '\t' '/^$@/ { print $$2 }' < index.tsv ) | $(AWK) -v tar=$@ 'BEGIN { OFS="\t" } { print tar,$$0 }' > $@.contents
%.tar:
	$(TAR) -cvf $@ --numeric-owner -T <( $(AWK) -F '\t' '/^$@/ { print $$2 }' < index.tsv ) | $(AWK) -v tar=$@ 'BEGIN { OFS="\t" } { print tar,$$0 }' > $@.contents
%.zip:
	bsdtar -cvf $@ --numeric-owner -T <( $(AWK) -F '\t' '/^$@/ { print $$2 }' < index.tsv ) | $(AWK) -v tar=$@ 'BEGIN { OFS="\t" } { print tar,$$0 }' > $@.contents

search: index.tsv
	@[ ! -z "$(pattern)" ] 
	grep $(pattern) index.tsv

extract: index.tsv
	$(eval TARFILES=$(shell $(AWK) -F '\t' '/$(pattern)/ { print $$1 }' index.tsv | uniq ))
	@for tf in $(TARFILES) ; do \
	$(TAR) -C $(dst) -xf $$tf $(pattern) ; \
	done

tars: $(TARFILES)

test: tars
	cat *.contents | cmp index.tsv -

# TODO does this work?
contenttest: tars
	@for tf in $(TARFILES) ; do \
	  cmp <( $(TAR) tf $$tf ) <( $(AWK) -v tar=$$tf -F '\t' "/^tar/ { print $$2 }" ) ; \
	done | cmp index.tsv -

upload: test
	@rsync -avR $(TARFILES) index.tsv Makefiles $(dst)

paratars:
	@parallel --slf $(PARAHOSTS) --nonall uptime
	parallel $(PARAOPTS) --slf $(PARAHOSTS) --joblog paratars.log make {} ::: $(TARFILES)

parupload: test
	parallel --jobs 4 -N 10 rsync -avR {} $(dst) ::: index.tsv Makefile $(TARFILES)

paracontenttest:
	@parallel $(PARAOPTS) --slf $(PARAHOSTS) --nonall uptime
	parallel $(PARAOPTS) --slf $(PARAHOSTS) --joblog paratest.log "cmp <( $(TAR) -tf {} ) <( $(AWK) -F '\t' '/^{}/ { print \$$2 }' index.tsv )" ::: $(TARFILES)

parextract:
	$(AWK) -F '\t' '/$(pattern)/ { print $$1 }' | uniq | parallel $(TAR) -C $(dst) -xf {} $(pattern)

_MAKEFILE_

# Find the size of all files. The awk command escapes ascii > 127.
find "$RELIN" -type f -exec $DUINK "{}" \; | make_listfiles | awk 'BEGIN{FS="";for(n=0;n<256;n++)ord[sprintf("%c",n)]=n}{for(i=1;i<=NF;i=i+1) { if(ord[$i]>127) { printf("\\%o",ord[$i])} else { printf("%s",$i) }};printf("\n")}' >  $OUT/index.tsv
  
if [ -e "$OUT/errors.log" ]; then
  echo "Warning: Errors logged:"
  cat "$OUT/errors.log"
fi

# Run the make job if required
if [ $JOBS -gt 0 ]; then
	$MAKE -C $OUT -Oline -j$JOBS tars
fi
